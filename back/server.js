const express = require('express');
const app = express();
const cors = require('cors');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const passport = require('passport');

// Middleware
app.use(express.json());
app.use(cors());
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(passport.initialize());


// Binding routes
app.use('/cryptos', require('./routes/cryptos.js'));
app.use('/users', require('./routes/users'));

// Routes
app.get('/', (req, res) => {
  res.send('This is the backend of our T-Web project ;)');
});

// Start the server
const port = process.env.PORT || 8080;

app.listen(port, function () {
  console.log('Backend listening on port ' + port);
})
