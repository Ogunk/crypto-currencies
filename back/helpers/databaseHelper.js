const { Client } = require('pg');

module.exports = {
    getClient() {
        return new Client({
            user: 'postgres',
            host: 'db',
            database: 'postgres',
            password: '',
            port: 5432,
          })
    }
}