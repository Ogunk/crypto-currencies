const express = require('express');
const router = require('express-promise-router')();
const passport = require('passport');
const passportConf = require('../passport');

const UsersController = require('../controllers/users');
const { validateBody, schemas } = require('../helpers/routeHelpers');

const passportSignIn = passport.authenticate('local', {session: false});
const passportJWT = passport.authenticate('jwt', {session: false});

router.route('/register')
.post(validateBody(schemas.signUp), UsersController.signUp);

router.route('/login')
.post(validateBody(schemas.signIn), passportSignIn, UsersController.signIn);

// oauth with google
router.route('/auth/google')
.post(passport.authenticate('google-token'), UsersController.signIn);

router.route('/profile')
.get(passportJWT, UsersController.getProfile);

router.route('/profile')
.post(passportJWT, UsersController.updateProfile);

module.exports = router;