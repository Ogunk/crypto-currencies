const router = require('express-promise-router')();
const axios = require('axios');
const passport = require('passport');
const passportConf = require('../passport');
const passportJWT = passport.authenticate('jwt', { session: false });
var UserController = require('../controllers/users');
var CryptoController = require('../controllers/cryptos');
const { DEFAULT_CURRENCY } = require('../configuration/index');
const cryptoModel = require('../models/crypto');

// Setting const variables for the crypto API
const CRYPTO_URL = 'https://min-api.cryptocompare.com';
const API_KEY = 'Apikey daedd73c084709440a8ba4bb4acd817a7afe85c334fb0b963b52f3b6b33dcbf3';
const API_HEADERS = { headers: { 'Authorization': API_KEY } };

/*** 
    List of routes 
***/

// Get the top trending currencies
router.route('/topCoins').get(async (req, res) => {

    var currency = DEFAULT_CURRENCY;

    if (req.headers.authorization) {
        try {
            currency = await UserController.getPreferedCurrencyFromTokenAsync(req.headers.authorization)
        } catch (err) {
            // token not valid or error encoutered -> using default
        }
    }

    const TOP_COINS_ENDPOINT = '/data/top/totalvolfull?limit=10&tsym=' + currency;

    let listCurrencies;

    listCurrencies = await new Promise((resolve, reject) => {
        axios.get(CRYPTO_URL + TOP_COINS_ENDPOINT, API_HEADERS)
            .then(data => {
                resolve(data);
            })
            .catch(error => {
                console.log(error);
            })
    });

    res.send(listCurrencies.data);

});


// Get the list of all currencies
router.get('/', async (req, res) => {

    const ALL_COINS_ENDPOINT = '/data/all/coinlist';

    let listCurrencies;

    listCurrencies = await new Promise((resolve, reject) => {
        axios.get(CRYPTO_URL + ALL_COINS_ENDPOINT, API_HEADERS)
            .then(data => {
                resolve(data);
            })
            .catch(error => {
                console.log(error);
            })
    });

    res.send(listCurrencies.data);

});


// Get the list of all currencies
router.get('/light/all', async (req, res) => {

    const ALL_COINS_ENDPOINT = '/data/all/coinlist';

    let listCurrencies;

    listCurrencies = await new Promise((resolve, reject) => {
        axios.get(CRYPTO_URL + ALL_COINS_ENDPOINT, API_HEADERS)
            .then(data => {
                var cryptos = [];
                for (var crypto in data.data.Data) {
                    cryptos.push({ name: crypto, cryptoId: data.data.Data[crypto].Id })
                }
                cryptos.sort(function (a, b) { return (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0); });
                resolve(cryptos);
            })
            .catch(error => {
                console.log(error);
            })
    });

    res.send(listCurrencies);

});


// Get the list of all currencies available on this wesite
router.get('/light', async (req, res) => {

    const ALL_COINS_ENDPOINT = '/data/all/coinlist';

    let listCurrencies;

    listCurrencies = await new Promise((resolve, reject) => {
        axios.get(CRYPTO_URL + ALL_COINS_ENDPOINT, API_HEADERS)
            .then(data => {
                var cryptos = [];
                for (var crypto in data.data.Data) {
                    cryptos.push({ name: crypto, cryptoId: data.data.Data[crypto].Id })
                }
                cryptos.sort(function (a, b) { return (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0); });
                resolve(cryptos);
            })
            .catch(error => {
                console.log(error);
            })
    });

    var cryptos = (await cryptoModel.getAllAsync()).rows;

    listCurrencies = listCurrencies.filter(c => cryptos.some(cr => cr.Name === c.name));

    res.send(listCurrencies);

});

router.route('/list')
    .get(passportJWT, CryptoController.get);

router.route('/')
    .post(passportJWT, UserController.isAdministrator, CryptoController.add);

router.route('/')
    .delete(passportJWT, UserController.isAdministrator, CryptoController.remove);



router.route('/:id').get(passportJWT, async (req, res, next) => {

    var currency = DEFAULT_CURRENCY;

    if (req.headers.authorization) {
        try {
            currency = await UserController.getPreferedCurrencyFromUserIdAsync(req.user.Id);
        } catch (err) {
            // token not valid, should not be the case as this root must be for authenticated users
        }
    }

    // Endpoints const VAR
    const ALL_COINS_ENDPOINT = '/data/all/coinlist?tsyms=' + currency;
    const COIN_CURRENT_VALUE_ENDPOINT = '/data/price?fsym=' + req.params.id + '&tsyms=' + currency;
    const COIN_DAY_HISTORY_ENDPOINT = '/data/v2/histoday?fsym=' + req.params.id + '&tsym=' + currency + '&limit=1';

    let cryptoId = req.params.id;
    let listCurrencies;

    // Getting the informations about the crypto currencie we need
    listCurrencies = await new Promise((resolve, reject) => {
        axios.get(CRYPTO_URL + ALL_COINS_ENDPOINT, API_HEADERS)
            .then(data => {
                resolve(data.data.Data[cryptoId]);
            })
            .catch(error => {
                console.log(error);
            })
    });

    // Getting the current value of the selected currencie
    let currentValue = await new Promise((resolve, reject) => {
        axios.get(CRYPTO_URL + COIN_CURRENT_VALUE_ENDPOINT, API_HEADERS)
            .then(data => {
                resolve(data);
            })
            .catch(error => {
                console.log(error);
            })
    });

    // Putting the current value in the json object to send to the front
    listCurrencies["currentValue"] = currentValue.data[currency];

    // Getting the prices of the day
    let dayHistory = await new Promise((resolve, reject) => {
        axios.get(CRYPTO_URL + COIN_DAY_HISTORY_ENDPOINT, API_HEADERS)
            .then(data => {
                resolve(data.data.Data);
            })
            .catch(error => {
                console.log(error);
            })
    });

    // Putting the day history datas about the currencie in the json object
    listCurrencies["dayHistory"] = dayHistory;

    // Giving the currency simbole back:
    if (currency === 'USD')
        listCurrencies["currencySymbole"] = '$';
    if (currency === 'EUR')
        listCurrencies["currencySymbole"] = '€';
    if (currency === 'BTC')
        listCurrencies["currencySymbole"] = 'Ƀ';


    // Sending the data to the front
    res.send(listCurrencies);
});

router.get('/:id/history/:period', async (req, res, next) => {

    var currency = DEFAULT_CURRENCY;

    if (req.headers.authorization) {
        try {
            currency = await UserController.getPreferedCurrencyFromTokenAsync(req.headers.authorization)
        } catch (err) {
            // token not valid or error encoutered -> using default
        }
    }

    const DAILY_COINS_HISTORY_ENDPOINT = '/data/v2/histoday?fsym=' + req.params.id + '&tsym=' + currency + '&limit=59';
    const HOURLY_COINS_HISTORY_ENDPOINT = '/data/v2/histohour?fsym=' + req.params.id + '&tsym=' + currency + '&limit=47';
    const MINUTE_COINS_HISTORY_ENDPOINT = '/data/v2/histominute?fsym=' + req.params.id + '&tsym=' + currency + '&limit=119';

    let historyDetails;

    switch (req.params.period) {
        case 'daily':
            historyDetails = await new Promise((resolve, reject) => {
                axios.get(CRYPTO_URL + DAILY_COINS_HISTORY_ENDPOINT, API_HEADERS)
                    .then(data => {
                        resolve(data);
                    })
                    .catch(error => {
                        console.log(error);
                    })
            });
            break;
        case 'hourly':
            historyDetails = await new Promise((resolve, reject) => {
                axios.get(CRYPTO_URL + HOURLY_COINS_HISTORY_ENDPOINT, API_HEADERS)
                    .then(data => {
                        resolve(data);
                    })
                    .catch(error => {
                        console.log(error);
                    })
            });
            break;
        case 'minute':
            historyDetails = await new Promise((resolve, reject) => {
                axios.get(CRYPTO_URL + MINUTE_COINS_HISTORY_ENDPOINT, API_HEADERS)
                    .then(data => {
                        resolve(data);
                    })
                    .catch(error => {
                        console.log(error);
                    })
            });
            break;
    }

    res.send(historyDetails.data);
});

module.exports = router;