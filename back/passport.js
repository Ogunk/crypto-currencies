const passport = require('passport');

const JwtStrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');
const { JWT_SECRET, GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET } = require('./configuration/index');
const userModel = require('./models/user');

const LocalStrategy = require('passport-local').Strategy;
const GoogleTokenStrategy = require('passport-google-token').Strategy;

// GOOGLE OAUTH2.0 STRATEGY
passport.use(new GoogleTokenStrategy({
  clientID: GOOGLE_CLIENT_ID,
  clientSecret: GOOGLE_CLIENT_SECRET
}, async (accessToken, refreshToken, profile, done) => {
  try{
  
    // Find the user specified in token
    var userRequest = await userModel.getUserByGoogleIdAsync(profile.id);

    // If user doesn't exist, create it
    if(userRequest.rowCount <= 0)
    {
      var newUser = {
        name: profile.displayName,
        email: profile.emails[0].value, 
        password: null, 
        googleId: profile.id
      }
      await userModel.createAsync(newUser);
      userRequest = await userModel.getUserByGoogleIdAsync(profile.id);
    }

    // return the user
    const user = userRequest.rows[0];
    done(null, user);
  }
  catch(error)
  {
    done(error, false, error.message);
  }
}));

// JSON WEB TOEKN STRATEGY
passport.use(new JwtStrategy({
  jwtFromRequest: ExtractJwt.fromHeader('authorization'),
  secretOrKey: JWT_SECRET
}, async (payload, done) => {
  try{
    // Find the user specified in token
    const userRequest = await userModel.getUserByIdAsync(payload.sub);

    // If user doesn't exist, handle it
    if(userRequest.rowCount <= 0)
    {
      return done(null, false);
    }

    // Otherwise, return the user
    const user = userRequest.rows[0];
    done(null, user);
  }
  catch(error){
    done(error, false);
  }
}));


// LOCAL STRATEGY
passport.use(new LocalStrategy({
  usernameField: 'email'
}, async (email, password, done) => {
  try {
    
    // Find the user specified in token
    const userRequest = await userModel.getUserByEmailAsync(email);

    // If user doesn't exist, handle it
    if(userRequest.rowCount <= 0)
    {
      return done(null, false);
    }

    // Check if the password is correct
    var user = userRequest.rows[0];
    var isPasswordValid = await userModel.isPasswordValidAsync(user, password);

    // If not, handle it
    if(!isPasswordValid)
    {
      return done(null, false);
    }

    // Otherwise, return the user
    done(null, user); 
  }
  catch (error) {
    done(error, false);
  }
}));

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});