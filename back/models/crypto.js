const dbHelper = require('../helpers/databaseHelper');
const bcrypt = require('bcryptjs');

module.exports = {
    async getAllAsync() {
        return new Promise(async (resolve, reject) => {
            try{
                var client = dbHelper.getClient();
                client.connect();
                var result = await client.query('SELECT * FROM public."Cryptos"');
                client.end();
                resolve(result);
            }
            catch(err)
            {
                reject(err)
            }
        });
    },

    async getByCryptoIdAsync(cryptoId) {
        return new Promise(async (resolve, reject) => {
            try{
                var client = dbHelper.getClient();
                client.connect();
                var result = await client.query('SELECT * FROM public."Cryptos" WHERE "CryptoId"=$1', [cryptoId]);
                client.end();
                resolve(result);
            }
            catch(err)
            {
                reject(err)
            }
        });
    },

    async addAsync(cryptoId, name) {
        return new Promise(async (resolve, reject) => {
            try{
                var client = dbHelper.getClient();
                client.connect();
                await client.query('INSERT INTO public."Cryptos"("CryptoId", "Name") VALUES($1, $2)', [cryptoId, name]);
                client.end();
                resolve();
            }
            catch(err)
            {
                reject(err)
            }
        });
    },

    async deleteAsync(cryptoId) {
        return new Promise(async (resolve, reject) => {
            try{
                var client = dbHelper.getClient();
                client.connect();
                var result = await client.query('DELETE FROM public."Cryptos" WHERE "CryptoId"=$1', [cryptoId]);
                client.end();
                resolve(result);
            }
            catch(err)
            {
                reject(err)
            }
        });
    }
}

