const dbHelper = require('../helpers/databaseHelper');
const bcrypt = require('bcryptjs');

module.exports = {
    async createAsync(newUser) {
        return new Promise(async (resolve, reject) => {
            try{
                // Hashing password (salt+hash) if password is present
                // No password in case of authentication with oauth
                if(newUser.password)
                {
                    const salt = await bcrypt.genSalt(10);
                    const passwordHash = await bcrypt.hash(newUser.password, salt);
                    newUser.password = passwordHash;
                }

                var client = dbHelper.getClient();
                client.connect();
                await client.query('INSERT INTO public."Users"("Name", "Email", "Password", "GoogleId") VALUES($1, $2, $3, $4)', [newUser.email, newUser.email, newUser.password, newUser.googleId]);
                client.end();
                resolve();
            }
            catch(err)
            {
                reject(err)
            }
        });
    },

    async getUserByEmailAsync(email) {
        return new Promise(async (resolve, reject) => {
            try{
                var client = dbHelper.getClient();
                client.connect();
                var result = await client.query('SELECT * FROM public."Users" WHERE "Email"=$1', [email]);
                client.end();
                resolve(result);
            }
            catch(err)
            {
                reject(err)
            }
        });
    },

    async getUserByIdAsync(id) {
        return new Promise(async (resolve, reject) => {
            try{
                var client = dbHelper.getClient();
                client.connect();
                var result = await client.query('SELECT * FROM public."Users" WHERE "Id"=$1', [id]);
                client.end();
                resolve(result);
            }
            catch(err)
            {
                reject(err)
            }
        });
    },

    async getUserByGoogleIdAsync(id) {
        return new Promise(async (resolve, reject) => {
            try{
                var client = dbHelper.getClient();
                client.connect();
                var result = await client.query('SELECT * FROM public."Users" WHERE "GoogleId"=$1', [id]);
                client.end();
                resolve(result);
            }
            catch(err)
            {
                reject(err)
            }
        });
    },

    async updateUserAsync(userId, name, defaultCurrency, preferedCrypto, newsKeywords) {
        return new Promise(async (resolve, reject) => {
            try{
                var client = dbHelper.getClient();
                client.connect();
                var result = await client.query('UPDATE public."Users" SET "Name"=$2, "DefaultCurrency"=$3, "PreferedCrypto"=$4, "NewsKeywords"=$5 WHERE "Id"=$1', [userId, name, defaultCurrency, preferedCrypto, newsKeywords]);
                client.end();
                resolve(result);
            }
            catch(err)
            {
                reject(err)
            }
        });
    },

    async isPasswordValidAsync(user, enteredPassword){
        return new Promise(async (resolve, reject) => {
            try {
                var result = await bcrypt.compare(enteredPassword, user.Password)
                resolve(result);
            }
            catch(error) {
                reject(new Error(error));
            }
        });
    }
}

