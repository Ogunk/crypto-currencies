const JWT = require('jsonwebtoken');
const userModel = require('../models/user');
const { JWT_SECRET } = require('../configuration/index');
var jwt = require('jsonwebtoken');

signToken = user => {
    // Generate token
    return JWT.sign({
        iss: 'T-WEB-700-gr-11',
        sub: user.Id,
        userName: user.Name,
        email: user.Email,
        isAdministrator: user.IsAdministrator,
        iat: new Date().getTime(),
        exp: new Date().setDate(new Date().getDate()+1)
    }, JWT_SECRET);
}


module.exports = {
    signUp: async (req, res, next) => {
        const { name, email, password , googleId} = req.value.body;

        // Check if there is a user with the same email
        const foundUserCount = (await userModel.getUserByEmailAsync(email)).rowCount;
        if(foundUserCount > 0)
        {
            res.status(403)
            res.json({error: "Email is already in use."});
            return;
        }

        // Create and get new user object
        var newUser = {
            name,
            email,
            password,
            googleId
        };
        await userModel.createAsync(newUser);
        newUser = (await userModel.getUserByEmailAsync(email)).rows[0];
        
        // Respond with token
        const token = signToken(newUser);
        res.status(200).json({token});
    },

    signIn: async (req, res, next) => {
        // Generate token
        const token = signToken(req.user);
        res.status(200).json({token});
    },

    getProfile: async (req, res, next) => {
        const user = (await userModel.getUserByIdAsync(req.user.Id)).rows[0];
        delete user.Password;
        res.status(200).json({user})
    },

    updateProfile: async (req, res, next) => {
        const { name, defaultCurrency, preferedCrypto, newsKeywords} = req.body;

        await userModel.updateUserAsync(req.user.Id, name, defaultCurrency, preferedCrypto, newsKeywords);
        const user = (await userModel.getUserByIdAsync(req.user.Id)).rows[0];

        res.status(200).json({user})
    },

    isAdministrator: async (req, res, next) => {
        if(req.user.IsAdministrator)
        {
            next();
        }
        else
        {
            res.status(401).send('Unauthorized');
        }
    },

    getPreferedCurrencyFromTokenAsync: (token) => {
        return new Promise(async (resolve, reject) => {
            try{
                var decoded = jwt.verify(token, JWT_SECRET);
                var user = (await userModel.getUserByIdAsync(decoded.sub)).rows[0];
                resolve(user.DefaultCurrency);
            }
            catch(err)
            {
                reject(err);
            }
        });
    },

    getPreferedCurrencyFromUserIdAsync: (userId) => {
        return new Promise(async (resolve, reject) => {
            try{
                var user = (await userModel.getUserByIdAsync(userId)).rows[0];
                resolve(user.DefaultCurrency);
            }
            catch(err)
            {
                reject(err);
            }
        });
    }
}