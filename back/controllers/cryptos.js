const JWT = require('jsonwebtoken');
const cryptoModel = require('../models/crypto');

module.exports = {
    get: async (req, res, next) => {
        var cryptos = (await cryptoModel.getAllAsync()).rows;

        cryptos.sort(function (a, b) { return (a.Name > b.Name) ? 1 : ((b.Name > a.Name) ? -1 : 0); });
        
        // Respond with cryptos
        res.status(200).json({cryptos});
    },

    add: async (req, res, next) => {
        const { cryptoId, name } = req.body;

        // Check if the crypto is already added
        const foundCryptoCount = (await cryptoModel.getByCryptoIdAsync(cryptoId)).rowCount;
        if(foundCryptoCount > 0)
        {
            res.status(403)
            res.json({error: "Crypto already added."});
            return;
        }

        // Create new crypto object and get full list
        await cryptoModel.addAsync(cryptoId, name);
        var cryptos = (await cryptoModel.getAllAsync()).rows;
        
        // Respond with cryptos
        res.status(200).json({cryptos});
    },

    remove: async (req, res, next) => {
        const { cryptoId } = req.body;

        // Check if the crypto is already added
        const foundCryptoCount = (await cryptoModel.getByCryptoIdAsync(cryptoId)).rowCount;
        if(foundCryptoCount <= 0)
        {
            res.status(403)
            res.json({error: "Crypto does not exists."});
            return;
        }

        // Delete crypto object and get full list
        await cryptoModel.deleteAsync(cryptoId);
        var cryptos = (await cryptoModel.getAllAsync()).rows;
        
        // Respond with cryptos
        res.status(200).json({cryptos});
    }
}