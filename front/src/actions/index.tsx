import axios from 'axios';
import { AUTH_SIGN_UP, AUTH_ERROR, AUTH_SIGN_OUT, AUTH_SIGN_IN } from './types';

export const oauthGoogle = (token: string) => {
    return async (dispatch: any) => {
        const res = await axios.post('http://localhost:8080/users/auth/google', {
            access_token: token
        });

        // Dispatch user just signed up
        dispatch({
            type: AUTH_SIGN_UP,
            payload: res.data.token
        });

        // Save the jwtToken into our localStorage
        localStorage.setItem("JWT_TOKEN", res.data.token);
    }
}  

export const signUp = (data: any) => {
    return async (dispatch: any) => {
        try{
            // Use the data and make HTTP request to our backend and send it along
            const res = await axios.post('http://localhost:8080/users/register', data);

            // Dispatch user just signed up
            dispatch({
                type: AUTH_SIGN_UP,
                payload: res.data.token
            });

            // Save the jwtToken into our localStorage
            localStorage.setItem("JWT_TOKEN", res.data.token);
        }catch(err)
        {
            dispatch({
                type: AUTH_ERROR,
                payload: "Email is already in use"
            });
            console.error(err);
        }
    }
}

export const signIn = (data: any) => {
    return async (dispatch: any) => {
        try{
            // Use the data and make HTTP request to our backend and send it along
            const res = await axios.post('http://localhost:8080/users/login', data);

            // Dispatch user just signed up
            dispatch({
                type: AUTH_SIGN_IN,
                payload: res.data.token
            });

            // Save the jwtToken into our localStorage
            localStorage.setItem("JWT_TOKEN", res.data.token);
        }catch(err)
        {
            dispatch({
                type: AUTH_ERROR,
                payload: "Email and password combination isn't valid"
            });
            console.error(err);
        }
    }
}

export const signOut = () => {
    return (dispatch: any) => {
        localStorage.removeItem('JWT_TOKEN');

        dispatch({
            type: AUTH_SIGN_OUT,
            payload: ''
        });
    }
}


export const updateProfile = (data: any) => {
    return async (dispatch: any) => {
        try{
            await axios.post('http://localhost:8080/users/profile', data, {
                headers: {
                    Authorization: localStorage.getItem('JWT_TOKEN')
                }
            })
        }catch(err)
        {
            console.error(err);
        }
    }
}