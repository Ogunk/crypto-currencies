import React from 'react';
import axios from 'axios';
import '../componentsCSS/Tab.css';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from 'react-router-dom';
import CoinList from './CoinList';
import CoinDetails from './CoinDetails';

type MyProps = {
};

type MyState = {
    topCoins: {},
    cryptos: {},
    currentTab: string
};

class Tab extends React.Component<MyProps, MyState> {

    constructor(props: MyProps) {
        super(props);
        this.state = {
            topCoins: {},
            cryptos: {},
            currentTab: 'TOP'
        }

        this.handleShowTopCoins = this.handleShowTopCoins.bind(this);
        this.handleShowAllCoins = this.handleShowAllCoins.bind(this);
        this.handleShowNews = this.handleShowNews.bind(this);
    }

    // Getting the data when the component is Mounted
    // Setting the state once the data are fetched
    componentDidMount() {
        this.handleShowTopCoins();
    }

    handleShowTopCoins() {
        this.setState({
            ...this.state,
            currentTab: 'TOP'
        });
        axios.get('http://localhost:8080/cryptos/topCoins', {
            headers: {
                Authorization: localStorage.getItem('JWT_TOKEN')
            }
        })
            .then(data => {
                if (data.hasOwnProperty("data")) {
                    this.setState(state => ({
                        topCoins: data.data.Data
                    }));
                }
            })
            .catch(error => {
                console.log(error);
            });
    }

    handleShowAllCoins() {
        this.setState({
            ...this.state,
            currentTab: 'OUR'
        });
        axios.get('http://localhost:8080/cryptos/light')
            .then(data => {
                if (data.hasOwnProperty("data")) {
                    this.setState(state => ({
                        cryptos: data.data
                    }));
                }
            })
            .catch(error => {
                console.log(error);
            });
    }

    handleShowNews() {
        this.setState({
            ...this.state,
            currentTab: 'NEWS'
        });
    }

    render() {

        return (
            <Router>
                <div className="tabsDiv">
                    <Link to="/">
                        <button onClick={this.handleShowTopCoins} className={(this.state.currentTab === 'TOP' ? 'activeButton' : 'disabledButton')}>Worlds top crypto</button>
                    </Link>
                    <Link to="/cryptos">
                        <button onClick={this.handleShowAllCoins} className={(this.state.currentTab === 'OUR' ? 'activeButton' : 'disabledButton')}>Our cryptos</button>
                    </Link>
                    <Link to="/news">
                        <button onClick={this.handleShowNews} className={(this.state.currentTab === 'NEWS' ? 'activeButton' : 'disabledButton')}>Crypto news</button>
                    </Link>
                </div>

                <Switch>
                    <Route exact path="/">
                        <CoinList coinList={this.state.topCoins} cryptos={false} />
                    </Route>
                    <Route exact path="/cryptos">
                        <CoinList coinList={this.state.cryptos} cryptos={true} />
                    </Route>
                    <Route exact path="/coinDetails">
                        <CoinDetails />
                    </Route>
                    <Route exact path="/news">
                        {/* TO DO */}
                    </Route>
                </Switch>

            </Router>
        )
    }
}

export default Tab;