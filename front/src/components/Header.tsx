import React from "react";
import siteLogo from "../images/siteLogo.png";
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import "../componentsCSS/Header.css";
import * as actions from '../actions';
import {AuthorizationService} from '../services/authorizationService';

class Header extends React.Component<any> {

  constructor(props: any){
    super(props);
    this.signOut = this.signOut.bind(this);
  }

  signOut() {
    this.props.signOut();
  }

  home(){
    var path = window.location.pathname; 
    console.log(path)
    if(path === '/' || path === '/cryptos' || path === '/news')
    {
      setInterval(() => {document.location.reload()}, 100);
    }
  }

  render() {
    return (
      <header className="header">
        <img className="siteLogoStyle" src={siteLogo} alt="siteLogo" />
        <Link to="/" className="siteTitle" onClick={this.home}>
          Crypto-Tech
        </Link>  
        {!this.props.isAuthenticated ?
        <React.Fragment>
          <Link to="/signIn" className="auth-btn">
            Login
          </Link>
          <Link to="/signUp" className="auth-btn">
            Register
          </Link>
        </React.Fragment> 
        : 
        <React.Fragment>
          {AuthorizationService.isUserAdmin() ? 
            <Link to="/admin" className="auth-btn">
              Administrate website
            </Link>
            : null
          }
          <Link to="/profile" className="auth-btn">
            Profile
          </Link>
          <Link to="/" className="auth-btn" onClick={this.signOut}>
            Log Out
          </Link>
        </React.Fragment> }

        
      </header>
    );
  }
}

function mapStateToProps(state: any)
{
  return {
    isAuthenticated: state.auth.isAuthenticated
  }
}

export default connect(mapStateToProps, actions)(Header);
