import React, { ChangeEvent } from "react";
import ReactMultiSelectCheckboxes from 'react-multiselect-checkboxes';
import "../componentsCSS/CustomInput.css";

interface IProps {
    input: {value: string, onChange:(e:React.ChangeEvent<HTMLInputElement>)=>void},
    id: string,
    label: string,
    fieldName: string | undefined,
    placeholder: string,
    type: string,
    values: any[] | undefined,
    currentlySelected: any[] | undefined,
    onChangeCallback: any
}

interface IState {
  currentlySelected: string | string[] | undefined
}

class CustomInput extends React.Component<IProps, IState> {

  state = {
    currentlySelected: this.props.currentlySelected
  }

  componentDidMount = async () => {
    this.setState({
      currentlySelected: this.props.currentlySelected
    })
  };

  render() {
    console.log()
    const { input: { value, onChange } } = this.props
    return (
      <div className="form-group">
        <label htmlFor={this.props.id}>{this.props.label}</label>
        {
          this.props.type === 'text' || this.props.type === 'password' ? 
          <input
              name = {this.props.fieldName}
              id = {this.props.id}
              placeholder = {this.props.placeholder}
              className="form-control"
              type={this.props.type}
              value = {value}
              onChange = {onChange}
          /> : null
        }
        {
          this.props.type === 'radio' ? 
            this.props.values?.map(v => { return(
              <div key={v.cryptoId}>
                <input key={v.cryptoId} type="radio" id={v} name={this.props.fieldName} value={v} checked={this.state.currentlySelected === v ? true : false} onChange={this.onRadioChange}/>
                <label key={v.cryptoId} htmlFor={v}>{v}</label>
              </div>)
            }) : null
        }
        {
          this.props.type === 'checkbox-multi' ? 
            this.props.values?.map(v => { return(
              <div key={v.cryptoId}>
                <input type="checkbox" id={v.cryptoId} name={v.cryptoId} value={v.cryptoId} checked={this.state.currentlySelected?.includes(v.cryptoId)} onChange={this.onCheckboxChange}/>
                <label htmlFor={v.cryptoId}>{v.name}</label>
              </div>)
            }) : null
        }
        {
          this.props.type === 'select-multiple' ? 
          <ReactMultiSelectCheckboxes options={this.props.values?.map(v => {return {label: v.Name, value: v.CryptoId, checked: true};})} onChange={this.onMultipleSelectChange} /> : null
        }


        
      </div>
    );
  }

  onRadioChange = (event: any) => {
    this.setState({
      ...this.state,
      currentlySelected: event.target.value
    });
    this.props.onChangeCallback(event.target.value);
  }

  onMultipleSelectChange = (values: {checked:boolean, label: string, value: string}[]) => {
    var value = values.filter(v => v.checked).map(v => v.value).join(' ');
    this.props.onChangeCallback(value);
  }

  onCheckboxChange = (value: any) => {
    this.props.onChangeCallback(value.target);
  }
}

export default CustomInput;
