import React from "react";
import {reduxForm, Field, change } from "redux-form";
import { connect } from 'react-redux';
import { compose } from 'redux';

import "../componentsCSS/Profile.css";
import CustomInput from "./CustomInput";
import * as actions from "../actions"; 
import { ApiService } from "../services/apiService";
import { Link } from "react-router-dom";

class Profile extends React.Component<any> {

  constructor(props: any) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
  }

  state = {
    name: undefined,
    defaultCurrency: undefined,
    preferedCrypto: undefined,
    newsKeywords: undefined,
    availableCryptos: undefined
  }

  componentDidMount = async () => {
    var profile = (await ApiService.getProfileInfo()).user;
    var availableCryptos = (await ApiService.getAllSelectedCrypto()).cryptos;

    this.props.dispatch(change('profile', 'name', profile.Name));
    this.props.dispatch(change('profile', 'newsKeywords', profile.NewsKeywords));

    this.setState({
      name: profile.Name,
      defaultCurrency: profile.DefaultCurrency,
      preferedCrypto: profile.PreferedCrypto,
      newsKeywords: profile.NewsKeywords,
      availableCryptos: availableCryptos
    })
    console.log(profile)
  };


  // Updating profile
  async onSubmit(formData: any){
    formData.defaultCurrency = this.state.defaultCurrency;
    formData.preferedCrypto = this.state.preferedCrypto;
    await this.props.updateProfile(formData);
  }

  onDefaultCurrencyUpdate = (value: string) => {
    this.setState({
      ...this.state,
      defaultCurrency: value
    })
  }

  onPreferedCryptoUpdate = (value: string) => {
    this.setState({
      ...this.state,
      preferedCrypto: value
    })
  }

  render() {
    console.log(this.state)
    const { handleSubmit} = this.props;
    return (
      <div className="row">
        <div className="col">
          <form className="profile-form" onSubmit={handleSubmit(this.onSubmit)}>
            <fieldset>
              <Field
                fieldName="name"
                name="name"
                type="text"
                id="name"
                label="Enter your name"
                placeholder="Bob"
                component={CustomInput} />
            </fieldset>
            {
              this.state.defaultCurrency ? 
              <fieldset>
                <Field
                  fieldName="defaultCurrency"
                  name="defaultCurrency"
                  type="radio"
                  id="defaultCurrency"
                  label="Choose your default currency"
                  values={['BTC','EUR', 'USD']}
                  currentlySelected={this.state.defaultCurrency}
                  component={CustomInput}
                  onChangeCallback={this.onDefaultCurrencyUpdate} />
              </fieldset>
              : null
            }
            {
              this.state.preferedCrypto !== undefined && this.state.preferedCrypto !== null ? 
              <fieldset>
                <Field
                  fieldName="preferedCrypto"
                  name="preferedCrypto"
                  type="select-multiple"
                  id="preferedCrypto"
                  label="Choose your prefered crypto currencies"
                  values={this.state.availableCryptos}
                  currentlySelected={this.state.preferedCrypto}
                  component={CustomInput}
                  onChangeCallback={this.onPreferedCryptoUpdate} />
              </fieldset>
            : null
            }
            <fieldset>
              <Field
                fieldName="newsKeywords"
                name="newsKeywords"
                type="text"
                id="newsKeywords"
                label="Keywords for your crypto-news (separated by a blank space)"
                placeholder="Bitcoin bubble hype"
                component={CustomInput} />
            </fieldset>

            { this.props.errorMessage ? 
            <div className="alert alert-danger">
              { this.props.errorMessage }
            </div>
            : null }

            <button type="submit" className="btn btn-success">Save</button>{' '}{' '}
            <Link to="/" className="btn btn-outline-warning">Back home</Link>
          </form>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state: any){
  return {
    errorMessage: state.auth.errorMessage
  }
}

export default compose<any>(
  connect(mapStateToProps, actions),
  reduxForm({ form: 'profile'})
)(Profile);
