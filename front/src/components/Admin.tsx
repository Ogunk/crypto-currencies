import React from "react";
import {reduxForm, Field } from "redux-form";
import { connect } from 'react-redux';
import { compose } from 'redux';

import "../componentsCSS/Admin.css";
import CustomInput from "./CustomInput";
import * as actions from "../actions"; 
import { ApiService } from "../services/apiService";
import { Link } from "react-router-dom";

class Admin extends React.Component<any> {
  state: {cryptos: any[] | undefined, displayedCryptos: any[] | undefined, selectedCryptos: any[] | undefined, currentIndex: number} = {
    cryptos: undefined,
    displayedCryptos: undefined,
    selectedCryptos: undefined,
    currentIndex: 0
  }

  componentDidMount = async () => {
    var cryptos = (await ApiService.getAllCryptoLightData());
    var selectedCryptos = (await ApiService.getAllSelectedCrypto()).cryptos;

    this.setState({
      cryptos,
      displayedCryptos: cryptos.slice(0, 100),
      selectedCryptos,
      currentIndex: 0
    })
  };

  onRadioUpdate = (element: any) => {
    if(!element.checked)
    {
      ApiService.removeCrypto(element.id);
    }
    else
    {
      ApiService.addCrypto(element.id, element.labels[0].innerText);
    }
    document.location.reload(true);
  }

  nextPage = () => {
    if(this.state.cryptos){
      var index = this.state.currentIndex + 1;
      if(index * 100 >= this.state.cryptos.length)
      {
        index = 0
      }
      this.setState({
        ...this.state,
        displayedCryptos: this.state.cryptos.slice(0+index*100, 100+index*100),
        currentIndex: index
      })
    }
  }

  previousPage = () => {
    if(this.state.cryptos){
      var index = this.state.currentIndex - 1;
      if(index < 0)
      {
        index = Math.floor(this.state.cryptos.length / 100);
      }
      this.setState({
        ...this.state,
        displayedCryptos: this.state.cryptos.slice(0+index*100, 100+index*100),
        currentIndex: index
      })
    }
  }

  render() {
    return (
      <div className="row">
        <div className="col" id="admin-form-col">
          <Link to="/" className="btn btn-outline-warning">Back home</Link>
          <br/>
          { this.state.displayedCryptos ?
            <React.Fragment>
              <button onClick={this.previousPage} className="btn btn-outline-primary">Previous</button>{' '}{' '}
              <button onClick={this.nextPage} className="btn btn-outline-primary">Next</button>
              <form className="admin-form">
                <fieldset>
                  <Field
                    fieldName="cryptos"
                    name="cryptos"
                    type="checkbox-multi"
                    id="cryptos"
                    label="Choose the cryptos to display on the website"
                    values={this.state.displayedCryptos}
                    currentlySelected={this.state.selectedCryptos?.map(c => c.CryptoId)}
                    component={CustomInput}
                    onChangeCallback={this.onRadioUpdate} />
                </fieldset>
                { this.props.errorMessage ? 
                  <div className="alert alert-danger">
                    { this.props.errorMessage }
                  </div>
                : null }
              </form>
            </React.Fragment> 
          : "Loading all cryptos..."} 
        </div>
      </div>
    );
  }
}

function mapStateToProps(state: any){
  return {
    errorMessage: state.auth.errorMessage
  }
}

export default compose<any>(
  connect(mapStateToProps, actions),
  reduxForm({ form: 'admin'})
)(Admin);
