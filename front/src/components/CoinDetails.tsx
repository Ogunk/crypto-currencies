import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useLocation } from 'react-router-dom';
import '../componentsCSS/CoinDetails.css';

type CoinDetailsProps = { details: any }
type CoinHistoryProps = { history: any }

const CoinDetails = () => {

    const [detailsState, setDetails] = useState<CoinDetailsProps>(
        {
            details: {}
        }
    );

    const [historyState, setHistory] = useState<CoinHistoryProps>(
        {
            history: {}
        }
    );

    let query = new URLSearchParams(useLocation().search);

    // On page load
    useEffect(() => {
        axios.get('http://localhost:8080/cryptos/' + query.get('coinName'), {
            headers: {
                Authorization: localStorage.getItem('JWT_TOKEN')
            }
        })
            .then(data => {
                if (data.hasOwnProperty("data")) {
                    setDetails({ details: data });
                }
            })
            .catch(error => {
                console.log(error);
            });

        axios.get('http://localhost:8080/cryptos/' + query.get('coinName') + '/history/daily', {
            headers: {
                Authorization: localStorage.getItem('JWT_TOKEN')
            }
        })
            .then(data => {
                setHistory({ history: data.data.Data });
            })
            .catch(error => {
                console.log(error);
            });
    }, []);

    // Creating the detail page when the datas are loaded
    function createCurrencieDetailsPage() {

        let startDate;
        let endDate;
        let valorization;

        if (detailsState.details.hasOwnProperty('data')) {
            startDate = new Date(detailsState.details.data.dayHistory.TimeFrom * 1000);
            endDate = new Date(detailsState.details.data.dayHistory.TimeTo * 1000);
            valorization = (((100 * detailsState.details.data.currentValue) / detailsState.details.data.dayHistory.Data[1].open) - 100);
        } else {
            return;
        }

        return (

            <div className="coinDetailsDiv">
                <div className="mainInfos">
                    <p className="coinDayHistory">General data from : {startDate.getDate() + "/" + startDate.getMonth() + "/" + startDate.getFullYear() + " "}
                        to {endDate.getDate() + "/" + endDate.getMonth() + "/" + endDate.getFullYear()}</p>
                    <div className="coinHeader">
                        <img className="headerImage" src={"https://www.cryptocompare.com/" + detailsState.details.data.ImageUrl} alt="coinImg"></img>
                        <h1 className="headerCoinName">{detailsState.details.data.CoinName}</h1>
                    </div>
                    <div className="coinValorization">
                        <p><b>Current price :</b> {detailsState.details.data.currentValue} {detailsState.details.data.currencySymbole} </p>
                        {valorization > 0 ?
                            <p style={{ color: "green" }}> ({Math.round(valorization * 100) / 100} %)</p>
                            :
                            <p style={{ color: "red" }}> ({Math.round(valorization * 100) / 100} %)</p>
                        }
                    </div>
                    <p><b>Today oppening price :</b> {detailsState.details.data.dayHistory.Data[1].open} {detailsState.details.data.currencySymbole}</p>
                    <p><b>Today lowest price : </b>{detailsState.details.data.dayHistory.Data[1].low} {detailsState.details.data.currencySymbole}</p>
                    <p><b>Today highest price : </b>{detailsState.details.data.dayHistory.Data[1].high} {detailsState.details.data.currencySymbole}</p>
                </div>
                <div className="periodInfo">
                    <h2>Periodic infos</h2>
                    <p>Select a period to get infos from : </p>
                    <select id="periods" onChange={handlePeriodChange}>
                        <option value="daily">Last 60 days</option>
                        <option value="hourly">Last 48 hours</option>
                        <option value="minute">Last 2 hours</option>
                    </select>
                    <div className="coinHistoryInfo">
                        {createPeriodDetail()}
                    </div>
                </div>
            </div>
        );
    };

    // Getting the data about the selected period
    function handlePeriodChange(event: any) {

        axios.get('http://localhost:8080/cryptos/' + query.get('coinName') + '/history/' + event.target.value, {
            headers: {
                Authorization: localStorage.getItem('JWT_TOKEN')
            }
        })
            .then(data => {
                setHistory({ history: data.data.Data });
            })
    };

    function createPeriodDetail() {

        let historyUI = [];

        historyUI.push(
            <div key="headerDiv" className="coinHistoryHeader">
                <ul key="header">
                    <li>Date</li>
                    <li>High</li>
                    <li>Low</li>
                    <li>Open</li>
                    <li>Close</li>
                </ul>
            </div>
        );

        let startDate = new Date(historyState.history.TimeFrom * 1000).toLocaleString();
        let endDate = new Date(historyState.history.TimeTo * 1000).toLocaleString();

        Object.keys(historyState.history.Data).forEach((key, index) => {
            let date = new Date(historyState.history.Data[key].time * 1000).toLocaleString();
            historyUI.push(
                <ul key={index}>
                    <li>{date}</li>
                    <li>{historyState.history.Data[key].high}{detailsState.details.data.currencySymbole}</li>
                    <li>{historyState.history.Data[key].low}{detailsState.details.data.currencySymbole}</li>
                    <li>{historyState.history.Data[key].open}{detailsState.details.data.currencySymbole}</li>
                    <li>{historyState.history.Data[key].close}{detailsState.details.data.currencySymbole}</li>
                </ul>
            )
        })

        return (
            <div>
                <p>Date from {startDate} to {endDate}</p>

                {historyUI}
            </div>
        )
    };

    return (
        <div>
            {localStorage.getItem('JWT_TOKEN') ? createCurrencieDetailsPage() : <h1>Please sign in or register to see this page</h1>}
        </div>
    )
}

export default CoinDetails;