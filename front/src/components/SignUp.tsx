import React from "react";
import {reduxForm, Field } from "redux-form";
import { connect } from 'react-redux';
import { compose } from 'redux';
import GoogleLogin from 'react-google-login';  

import "../componentsCSS/SignUp.css";
import CustomInput from "./CustomInput";
import * as actions from "../actions"; 

class SignUp extends React.Component<any> {

  constructor(props: any) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.responseGoole = this.responseGoole.bind(this);
  }

  // Sign Up using email and password
  async onSubmit(formData:{}){
    await this.props.signUp(formData);

    // Redirect to home
    if(!this.props.errorMessage) {
      this.props.history.push('/');
      document.location.reload(true);
    }
  }

  // Sign Up using google
  async responseGoole(res: any) {
    await this.props.oauthGoogle(res.accessToken);

    // Redirect to home
    if(!this.props.errorMessage) {
      this.props.history.push('/');
      document.location.reload(true);
    }
  }

  render() {
    const { handleSubmit} = this.props;
    return (
      <div className="row">
        <div className="col">
          <form className="register-form" onSubmit={handleSubmit(this.onSubmit)}>
            <fieldset>
              <Field
                fieldName="email"
                name="email"
                type="text"
                id="email"
                label="Enter your email"
                placeholder="example@epitech.eu"
                component={CustomInput} />
            </fieldset>
            <fieldset>
            <Field
                fieldName="password"
                name="password"
                type="password"
                id="password"
                label="Enter your password"
                placeholder="**********"
                component={CustomInput} />
            </fieldset>

            { this.props.errorMessage ? 
            <div className="alert alert-danger">
              { this.props.errorMessage }
            </div>
            : null }

            <button type="submit" className="btn btn-primary">Register</button>
          </form>
        </div>
        <div className="col">
          <div className="text-center login-form">
            <div className="alert alert-primary">
              Or register using Google
            </div>
            <GoogleLogin 
              clientId="711174110758-qo2gv5448pqqm8svmhmdhrb51nhb2gs8.apps.googleusercontent.com"
              buttonText="Google"
              onSuccess={this.responseGoole}
              onFailure={this.responseGoole}
              />
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state: any){
  return {
    errorMessage: state.auth.errorMessage
  }
}

export default compose<any>(
  connect(mapStateToProps, actions),
  reduxForm({ form: 'signup'})
)(SignUp);
