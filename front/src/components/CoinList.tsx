import React from 'react';
import "../componentsCSS/CoinList.css";
import { Link } from 'react-router-dom';

type CoinListProps = { coinList: any, cryptos: boolean };

const CoinList = ({ coinList, cryptos }: CoinListProps) => {

    var coinsList: any = [];

    // Itterating through the Object cointaining the datas and creating the list
    if (!cryptos) {
        coinsList = [];

        coinsList.push(
            <div key="headerDiv" className="coinListHeader">
                <ul key="header">
                    <li>#</li>
                    <li>Nom</li>
                    <li>Market Cap</li>
                    <li>Price</li>
                    <li>Volume 24h</li>
                    <li>Circulating supply</li>
                    <li>Change 24h</li>
                </ul>
            </div>
        );

        Object.keys(coinList).forEach((key, index) => {
            coinsList.push(
                <Link key={index} to={"/coinDetails?coinName=" + coinList[index].CoinInfo["Name"]} >
                    <ul key={index}>
                        <li> <b>{index + 1} </b></li>
                        <li>{coinList[index].CoinInfo["FullName"]}</li>
                        <li>{Math.round(coinList[index].RAW[Object.keys(coinList[index].RAW)[0]]["MKTCAP"])}</li>
                        <li>{(Math.round(coinList[index].RAW[Object.keys(coinList[index].RAW)[0]]["PRICE"] * 100) / 100)} {coinList[index].DISPLAY[Object.keys(coinList[index].DISPLAY)[0]]["TOSYMBOL"]}</li>
                        <li>{(Math.round(coinList[index].RAW[Object.keys(coinList[index].RAW)[0]]["VOLUME24HOUR"] * 100) / 100)}</li>
                        <li>{(Math.round(coinList[index].RAW[Object.keys(coinList[index].RAW)[0]]["SUPPLY"] * 100) / 100)}</li>
                        <li>{(Math.round(coinList[index].RAW[Object.keys(coinList[index].RAW)[0]]["CHANGE24HOUR"] * 100) / 100)}</li>
                    </ul>
                </Link>
            )
        });
    } else {
        coinsList = [];

        coinsList.push(
            <div className="coinListHeader" key="divHeader">
                <ul key="header">
                    <li className="allCoinsHeader">List of coins</li>
                </ul>
            </div>
        );

        Object.keys(coinList).forEach((key, index) => {
            coinsList.push(
                <Link key={index} to={"/coinDetails?coinName=" + coinList[key].name} >
                    <ul key={index}>
                        <li>{coinList[key].name}</li>
                    </ul>
                </Link>
            )
        })
    }

    return (
        <div>
            {(Object.entries(coinsList).length ?
                <div className="coinsInfos">{coinsList} </div>
                :
                <h1>Loading data ...</h1>
            )}
        </div>
    )
};

export default CoinList;