import jwtDecode from 'jwt-decode';

export class AuthorizationService {
  public static isUserAdmin() {
    const jwtToken = localStorage.getItem('JWT_TOKEN');
    if(jwtToken)
    {
        var decoded = jwtDecode<any>(jwtToken);
        return decoded.isAdministrator;
    }
    else
    {
        return false;
    }
    
  }
}
