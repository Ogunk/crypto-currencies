import axios from 'axios';

export class ApiService {
  public static async getProfileInfo() {
    return new Promise<any>(async (resolve, reject) => {
      try {
        const response = await axios.get('http://localhost:8080/users/profile', {
            headers: {
                Authorization: localStorage.getItem('JWT_TOKEN')
            }
        });
        resolve(response.data);
      } catch (err) {
        reject(err);
      }
    });
  }

  public static async getAllCryptoLightData() {
    return new Promise<any>(async (resolve, reject) => {
      try {
        const response = await axios.get('http://localhost:8080/cryptos/light/all');
        resolve(response.data);
      } catch (err) {
        reject(err);
      }
    });
  }

  public static async getAllSelectedCrypto() {
    return new Promise<any>(async (resolve, reject) => {
      try {
        const response = await axios.get('http://localhost:8080/cryptos/list', {
          headers: {
            Authorization: localStorage.getItem('JWT_TOKEN')
          }
        });
        resolve(response.data);
      } catch (err) {
        reject(err);
      }
    });
  }

  public static async addCrypto(cryptoId: string, name: string) {
    return new Promise<any>(async (resolve, reject) => {
      try {
        const response = await axios.post('http://localhost:8080/cryptos', {
          cryptoId,
          name
        } ,{
          headers: {
            Authorization: localStorage.getItem('JWT_TOKEN')
          }
        });
        resolve(response.data);
      } catch (err) {
        reject(err);
      }
    });
  }

  public static async removeCrypto(cryptoId: string) {
    return new Promise<any>(async (resolve, reject) => {
      try {
        const response = await axios.delete('http://localhost:8080/cryptos', {
          data: {
            cryptoId
          },
          headers: {
            Authorization: localStorage.getItem('JWT_TOKEN')
          }
        });
        resolve(response.data);
      } catch (err) {
        reject(err);
      }
    });
  }
}
