import React from "react";
import "./App.css";
import Header from "./components/Header";

class App extends React.Component<{}> {
  render() {
    return (
      <React.Fragment>     
        <Header/>
        <div className="page-content">
          {this.props.children}
        </div>
      </React.Fragment>   
    );
  }
}

export default App;
