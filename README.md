# T-Web Groupe 11

The aim of this project is to provide a website where users can authenticate using email/password or their google account.
On the website, users can check the current valu of the most popular cryptos, brows a list of cryptos managed by an admin and consult the latest crypto news.


# Starting our application

1.   Start all containers with `docker-compose up`
2.   While docker-compose is running, run in an other terminal in the root directory of this project: 
```cat ./db/tables/dump_{Date_of_the_backup}.sql | docker exec -i {db_docker_container} psql -U postgres```
3.   Open http://localhost:3000

# Geneal infos

-   The API is located on port `8080`
-   The front is located on port `3000`
-   The Database is located on port `5432`
-   The Database web interface is located on port `5433`, you can login with user: `dev@localhost` and password: `1234`. To login to the database, set the hostname to: `db` and username: `postgres`


# Apply backup of the database (structure and data)

1. Delete the content of the db/psql directory.
2. While docker-compose is running, run in an other terminal in the root directory of this project: 
```cat ./db/tables/dump_{Date_of_the_backup}.sql | docker exec -i {db_docker_container} psql -U postgres```


# Make backup of the database (structure and data)

While docker-compose is running, run in an other terminal in the root directory of this project: 
```docker exec -t web_db_1 pg_dumpall -c -U postgres > ./db/tables/dump_``date +%d-%m-%Y"_"%H_%M_%S``.sql```

